AddCSLuaFile()

SWEP.Author			= "Mutoh"
SWEP.Base			= "weapon_base"
SWEP.PrintName		= "Ranged Weapon"

SWEP.ViewModel		= "models/weapons/v_crossbow.mdl"
SWEP.ViewModelFlip	= false
SWEP.UseHands		= true
SWEP.WorldModel		= "models/weapons/w_crossbow.mdl"
SWEP.SetHoldType 	= "crossbow"

SWEP.Weight			= 5
SWEP.AutoSwitchTo	= true

SWEP.Slot			= 2
SWEP.SlotPos		= 0

SWEP.DrawAmmo		= false
SWEP.DrawCrosshair	= false

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Automatic		= true
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 10
SWEP.Primary.NumShots		= 1
SWEP.Primary.Spread			= 0
SWEP.Primary.Cone			= 0
SWEP.Primary.Delay			= 1

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo			= "none"
SWEP.Secondary.Automatic	= false

SWEP.ShouldDropOnDie		= false

local ShootSound = Sound("Weapon_Crossbow.Single")

function SWEP:Initialize()
	self:SetHoldType( "crossbow" )
end

function SWEP:PrimaryAttack()

	local ply = self:GetOwner()
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	ply:LagCompensation( true )
	self:FireProjectile()
	ply:LagCompensation( false )

	ply:SetWalkSpeed(150)
	timer.Remove( "MovementPenaltyT" )
	timer.Create( "MovementPenaltyT", 1, 1, function() ply:SetWalkSpeed(275) end )

	self:Reload()

end

function SWEP:CanPrimaryAttack()
	return true
end

function SWEP:FireProjectile()

	local tr = self.Owner:GetEyeTrace()

	self:EmitSound(ShootSound)
	self.BaseClass.ShootEffects(self)

	if (!SERVER) then return end

	local ent = ents.Create("projectile_arrow")

	local ang = Angle(0, self.Owner:GetAngles().y, 0)
	local aim = ang:Forward()
	local wep = ang:Right() * 4 - ang:Up() * 24
	ent:SetPos(self.Owner:EyePos() + (aim * 32) + wep)
	ent:SetAngles(ang)
	ent:SetGravity(0)
	ent:SetOwner(self:GetOwner())
	ent:Spawn()

	local phys = ent:GetPhysicsObject()
	if !(phys && IsValid(phys)) then ent:Remove() return end
	phys:EnableGravity(false)
	phys:SetVelocity( aim * 800 )

	timer.Simple(.46, function()
		if ent != nil && IsValid(ent) then
			ent:Remove()
		end
	end)
end

function SWEP:Holster( wep )
	return false
end
