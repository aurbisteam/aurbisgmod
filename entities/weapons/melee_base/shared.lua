AddCSLuaFile()

SWEP.Author			= "Mutoh"
SWEP.Base			= "weapon_base"
SWEP.PrintName		= "Melee Weapon"

SWEP.ViewModel		= "models/weapons/v_stunstick.mdl"
SWEP.ViewModelFlip	= false
SWEP.UseHands		= true
SWEP.WorldModel		= "models/weapons/w_stunbaton.mdl"
SWEP.SetHoldType 	= "melee2"

SWEP.Weight			= 5
SWEP.AutoSwitchTo	= true

SWEP.Slot			= 2
SWEP.SlotPos		= 0

SWEP.DrawAmmo		= false
SWEP.DrawCrosshair	= false

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Automatic		= true
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 10
SWEP.Primary.NumShots		= 1
SWEP.Primary.Spread			= 0
SWEP.Primary.Cone			= 0
SWEP.Primary.Delay			= 1

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo			= "none"
SWEP.Secondary.Automatic	= false

SWEP.ShouldDropOnDie		= false

function SWEP:Initialize()

    self:SetWeaponHoldType( "melee2" )

end

function SWEP:PrimaryAttack()

    self.Weapon:SetNextPrimaryFire( CurTime() + 1 )

    local trace = self.Owner:GetEyeTrace();
    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SendWeaponAnim( ACT_VM_SWINGHARD )
        bullet = {}
        bullet.Num    = 1
        bullet.Src    = self.Owner:GetShootPos()
        bullet.Dir    = self.Owner:GetAimVector()
        bullet.Spread = Vector(0, 0, 0)
        bullet.Tracer = 0
        bullet.Force  = 1
        bullet.Damage = 25
        self.Owner:LagCompensation( true )
        self.Owner:FireBullets(bullet)
        self.Owner:LagCompensation( false )
        self.Weapon:EmitSound("Weapon_Crowbar.Melee_Hit")
    else
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SendWeaponAnim( ACT_VM_SWINGMISS )
        self.Weapon:EmitSound("Weapon_Crowbar.Single")
    end

    self.Owner:SetWalkSpeed(self.Owner:GetStat("speed")*.7)
	timer.Remove( "MovementPenaltyT" )
	timer.Create( "MovementPenaltyT", 1, 1, function() self.Owner:SetWalkSpeed(self.Owner:GetStat("speed")) end )
    self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
end

function SWEP:CanPrimaryAttack()
	return self.Owner:OnGround()
end

function SWEP:Holster( wep )
	return false
end
