AddCSLuaFile()

local model = "models/Items/CrossbowRounds.mdl"

ENT.Base = "base_anim"
ENT.Type = "anim"
ENT.RenderGroup	= RENDERGROUP_OPAQUE

function ENT:PhysicsCollide( data, physobj )
end


function ENT:PhysicsUpdate( physobj )
end

if ( CLIENT ) then

	function ENT:Draw()

		self:DrawModel()

	end

	hook.Add( "PreDrawHalos", "ArrowHalo", function()
		local arrow = {}

		table.insert(arrow, self)

		--halo.Add( arrow, Color( 0, 255, 0 ), 0, 0, 2, true, true )
	end )

end


if (SERVER) then

	function ENT:Initialize()

		self:SetModel(model)
		self:SetName("arrow")

		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:Activate()

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:Wake()
		end
	end

	function ENT:StartTouch( entity )
		entity:TakeDamage(10, self, self)
		self:Remove()
	end


	function ENT:EndTouch( entity )
	end


	function ENT:Touch( entity )
	end

end
