AddCSLuaFile()

local model = "models/hunter/tubes/tube2x2x4.mdl"

ENT.Type = "anim"
ENT.Base = "base_entity"

ENT.PrintName = "Tower"
ENT.RenderGroup	= RENDERGROUP_BOTH

local radius = 520

if SERVER then

local targets = {}
local speed = 1.3
local damage = 1
local nextShot = 0
local nextThink = 0
local thinkSpeed = .1

function ENT:Initialize()
	self:SetModel(model)
	self:PhysicsInit(SOLID_OBB)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_OBB)
	--self:SetModelScale(self:GetModelScale() * 0.12)
	self:Activate()
	self:SetMaxHealth(100)
	self:SetHealth(100)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then
		phys:Wake()
	end

end

function ENT:OnTakeDamage(dmgInfo)
	local dmg = dmgInfo:GetDamage()
	local health = self:Health()
	local maxHealth = self:GetMaxHealth()
	self:SetHealth( health - dmg )
	if (health - dmg) <= 0 then
		self:Remove()
	end
end

function ENT:Think()

	if nextThink < CurTime() then

		local e = ents.FindInSphere( self:GetPos(), radius )

		if table.Count(targets) > 0 then for k,v in pairs(targets) do
			if table.KeyFromValue(e, v) == nil or !v:IsValid() or !v:Alive() then
				table.remove(targets, k)
				self:SetNWEntity("target", self)
				--print(v:GetName() .. " removed. " .. self:GetNWEntity("target"):GetName())
			end
		end end

		for k,v in pairs(e) do
			if v:IsPlayer() then
				if table.KeyFromValue(targets, v) == nil and v:IsValid() and v:Alive() then
					table.insert(targets, v)
					self:SetNWEntity("target", targets[1])
					--print(v:GetName() .. " added. " .. self:GetNWEntity("target"):GetName())
				end
			end
		end

		nextThink = CurTime() + thinkSpeed

	end

	if nextShot < CurTime() then

		if targets[1] != nil and targets[1]:IsValid() then
			targets[1]:TakeDamage(damage, self, self)
			nextShot = CurTime() + speed
		end

	end

end

end

if CLIENT then

local mat = Material("cable/redlaser")
local w, h = 100, 10

function ENT:Draw()
    --self:SetRenderOrigin(Vector( 80, -1080, 150))
	self:DrawModel()
end

function ENT:DrawTranslucent()
	local ply = LocalPlayer()

	cam.Start3D2D( self:GetPos()+Vector(0,0,1), Angle(0, 0, 0), 1 )
		surface.DrawCircle( 0, 0, radius + 4, Color(55,255,55,255) )
	cam.End3D2D()

	local health = self:Health()
	local maxHealth = self:GetMaxHealth()
	local angpos = (ply:GetPos() - self:GetPos()):Angle()
	cam.Start3D2D(self:GetPos()+Vector(0,0,300), Angle(0, angpos.y, 0) + Angle(0, 90, 90), 1 )
		surface.SetDrawColor( 0, 0, 0, 200 )
		surface.DrawRect( -w/2 - 2, -2, w + 4, h + 4 )
		surface.SetDrawColor( 55, 255, 55 )
		surface.DrawRect( -w/2, 0, health, h )
	cam.End3D2D()

	render.DrawBox( self:GetPos()+Vector(0,0,376), ply:EyeAngles(), Vector(0, 0, 0), Vector(self.Health, 10, 0), Color(0, 255, 0), false )

	local t = self:GetNWEntity("target")
	if t != self and t:IsValid() then
		render.SetMaterial( mat )
		render.DrawBeam( self:GetPos() + Vector(0,0,256), t:GetPos() +  Vector(0,0,32), 2, 1, 0, Color(0,0,255) )
	end
end

end
