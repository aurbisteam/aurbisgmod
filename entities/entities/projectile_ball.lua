AddCSLuaFile()

local model = "models/XQM/Rails/gumball_1.mdl"

ENT.Base = "base_anim"
ENT.Type = "anim"
ENT.RenderGroup	= RENDERGROUP_OPAQUE

function ENT:PhysicsCollide( data, physobj )
end


function ENT:PhysicsUpdate( physobj )
end

if ( CLIENT ) then

	function ENT:Draw()
		self:DrawModel()
	end

end

if ( SERVER ) then

	function ENT:Initialize()

		self:EmitSound(Sound("WaterExplosionEffect.Sound"))

		self:SetModel(model)
		self:SetName("ball")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:Activate()

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:Wake()
		end
	end

	function ENT:StartTouch( entity )
		entity:TakeDamage(10, self, self)
		self:Remove()
	end


	function ENT:EndTouch( entity )
	end


	function ENT:Touch( entity )
	end

end
