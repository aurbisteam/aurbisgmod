AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 275
PLAYER.RunSpeed				= 350

local model = "models/player/soldier_stripped.mdl"

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:Give( "melee_base" )

end

function PLAYER:SetModel()
	util.PrecacheModel( model )
	self.Player:SetModel( model )
end

PLAYER.stats = {
	["hp"] = 100,
	["mp"] = 100,
	["ppow"] = 10,
	["mpow"] = 10,
	["as"] = 1,
	["pdef"] = 10,
	["mdef"] = 10,
	["speed"] = 275,
	["hps"] = 1,
	["mps"] = 1,
	["cdr"] = 0
}

PLAYER.lvlStats = {
	["hp"] = 10,
	["mp"] = 10,
	["ppow"] = 5,
	["mpow"] = 5,
	["as"] = .01,
	["pdef"] = 5,
	["mdef"] = 5,
	["speed"] = 0,
	["hps"] = .1,
	["mps"] = .1,
	["cdr"] = 0
}

PLAYER.spells = {
	[1] = "line",
	[2] = "area",
	[3] = "dash",
	[4] = "jump"
}

player_manager.RegisterClass( "talos", PLAYER, "player_default" )
