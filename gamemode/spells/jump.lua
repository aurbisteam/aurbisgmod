SPELL.Name		= "Jump";
SPELL.Icon		= "";
SPELL.Range		= -1;
SPELL.Cooldown	= 3;
SPELL.Time = 0;

local cost = 25

SPELL.OnInitalize = function()
end

SPELL.CanCast = function( ply )
    if ply:Mana() < cost then ply:ChatPrint("Not enough mana!") return false end

	if !ply:TargeterFree() then return false end

	return true
end

SPELL.OnCast = function( ply )
	if !SERVER then return end

	local RP = RecipientFilter()
	RP:AddAllPlayers()

	net.Start("au_anim")
	net.WriteEntity(ply)
	net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
	net.WriteInt(ACT_HL2MP_JUMP_CROSSBOW, 12)
	net.Send(RP)

    ply:AddMana(-cost)

	local targetpos = ply:GetTargeterPos()
    local posxy = ply:GetPosXY()

	ply:EmitSound(Sound("Weapon_Mortar.Incomming"))

	ply:SetVar("jp_state", true)
	ply:SetVar("jp_starttime", RealTime())
	ply:SetVar("jp_start", posxy)
	ply:SetVar("jp_end", targetpos)
	ply:SetVar("jp_height", 200)
	ply:SetVar("jp_speed", .75)

end
