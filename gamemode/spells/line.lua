SPELL.Name		= "Line"
SPELL.Icon		= ""
SPELL.Range		= -1
SPELL.Cooldown	= 1
SPELL.Time = 0

local cost = 10

SPELL.OnInitalize = function()
end

SPELL.CanCast = function( ply )
    if ply:Mana() < cost then return false end

	return IsValid(ply)
end

SPELL.OnCast = function( ply )
	if !SERVER then return end

	local RP = RecipientFilter()
	RP:AddAllPlayers()

	net.Start("au_anim")
	net.WriteEntity(ply)
	net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
	net.WriteInt(ACT_GMOD_GESTURE_ITEM_THROW, 12)
	net.Send(RP)

	timer.Simple(.46*2, function()
        local posxy = ply:GetPosXY()
		local aim = ply:GetYawVector()
        local ang = ply:GetYawAngle()

		local ent = ents.Create("projectile_ball")

		ent:SetPos(posxy + aim * 64 + ang:Up() * 48)
		ent:SetAngles(ang)
		ent:SetOwner(ply)
		ent:Spawn()

		local phys = ent:GetPhysicsObject()
		if !(phys && IsValid(phys)) then ent:Remove() return end
		phys:ApplyForceCenter( aim:GetNormalized() * 60000 )
		phys:EnableGravity(false)


		timer.Simple(.46, function()
			if ent != nil && IsValid(ent) then
				ent:Remove()
			end
		end)
	end)



end
