SPELL.Name		= "Spider";
SPELL.Icon		= "";
SPELL.Range		= -1;
SPELL.Cooldown	= 8;
SPELL.Time = 0;

SPELL.OnInitalize = function()
end

SPELL.CanCast = function( ply )
	return IsValid(ply)
end

SPELL.OnCast = function( ply )
	if !SERVER then return end

	local RP = RecipientFilter()
	RP:AddAllPlayers()

	net.Start("au_anim")
	net.WriteEntity(ply)
	net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
	net.WriteInt(ACT_GMOD_GESTURE_ITEM_THROW, 12)
	net.Send(RP)

	timer.Simple(.46*2, function()
    local pos = ply:GetPos();
  	local posxy = Vector( pos.x, pos.y, 1 )
  	local eye = ply:EyeAngles()
  	local ang = Angle(0, eye.y, 0)
  	local aim = ang:Forward()
  	local angx = eye.x
  	local m = math.Clamp(math.Remap(angx, 85, 15, 10, 400), 15, 400)
  	local targetpos = posxy + aim * m

		local ent = ents.Create("npc_headcrab")

		ent:SetPos(posxy + (aim * 64))
		ent:SetAngles(ang)
		ent:SetOwner(ply)
		ent:Spawn()

    local team1 = ply:Team()
    local team2 = team1 % 2 + 1

    --[[for _, x in pairs( player.GetAll() ) do --for every found entity do
  		if !x:IsPlayer() then return end -- if found entity is not NPC then do nothing
      if x:Team() == team1 then
    	  ent:AddEntityRelationship( x, D_HT, 99 ) -- self entity will like found entity
      elseif x:Team() == team2 then
        ent:AddEntityRelationship( x, D_HT, 99 ) -- self entity will hate found entity
      end
    end]]
	end)

end
