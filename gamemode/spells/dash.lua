SPELL.Name		= "Dash";
SPELL.Icon		= "";
SPELL.Range		= -1;
SPELL.Cooldown	= 1;
SPELL.Time = 0;

SPELL.OnInitalize = function()
end

SPELL.CanCast = function( ply )
	return IsValid(ply)
end

SPELL.OnCast = function( ply )
	if !SERVER then return end

	local RP = RecipientFilter()
	RP:AddAllPlayers()

	net.Start("au_anim")
	net.WriteEntity(ply)
	net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
	net.WriteInt(ACT_GMOD_GESTURE_RANGE_ZOMBIE, 12)
	net.Send(RP)

	local aim = ply:GetYawVector()

	ply:EmitSound(Sound("WeaponFrag.Throw"))

	ply:SetVar("dash_end", RealTime() + .8)
	ply:SetVar("dash_angle", aim)
	ply:SetVar("dash_speed", 600)

end
