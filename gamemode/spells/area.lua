SPELL.Name		= "Area";
SPELL.Icon		= "";
SPELL.Range		= 400;
SPELL.Cooldown	= 1;
SPELL.Cost      = 40;
SPELL.Time = 0;

local cost = 40

SPELL.OnInitalize = function()
end

SPELL.CanCast = function( ply )
    if ply:Mana() < cost then ply:ChatPrint("Not enough mana!") return false end

	if !ply:TargeterFree() then return false end

	return true
end

SPELL.OnCast = function( ply )
	if !SERVER then return end

	local RP = RecipientFilter()
	RP:AddAllPlayers()

	net.Start("au_anim")
	net.WriteEntity(ply)
	net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
	net.WriteInt(ACT_GMOD_GESTURE_ITEM_GIVE, 12)
	net.Send(RP)

    ply:AddMana(-cost)

	local targetpos = ply:GetTargeterPos()

	local explode = ents.Create( "env_explosion" )
	explode:SetPos( targetpos )
	explode:SetOwner( ply )
	explode:Spawn()
	explode:SetKeyValue( "iMagnitude", "20" )

	timer.Simple(.46*2, function()
		explode:Fire( "Explode", 0, 0 )
		explode:EmitSound( "weapon_AWP.Single", 400, 400 )
	end)

end
