include( "shared.lua" )

-- Includes

local function LoadModules()
    local root = GM.FolderName .. "/gamemode/modules/"
    local _, folders = file.Find(root .. "*", "LUA")

    for _, folder in SortedPairs(folders, true) do
        for _, File in SortedPairs(file.Find(root .. folder .. "/sh_*.lua", "LUA"), true) do
            include(root .. folder .. "/" .. File)
						print("cl_included " .. root .. folder .. "/" .. File)
        end

        for _, File in SortedPairs(file.Find(root .. folder .. "/cl_*.lua", "LUA"), true) do
            include(root .. folder .. "/" .. File)
						print("cl_included " .. root .. folder .. "/" .. File)
        end
    end
end
LoadModules()


if not CLIENT then return end

function draw.Circle( x, y, radius, seg )
	local cir = {}

	table.insert( cir, { x = x, y = y, u = 0.5, v = 0.5 } )
	for i = 0, seg do
		local a = math.rad( ( i / seg ) * -360 )
		table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
	end

	local a = math.rad( 0 ) -- This is needed for non absolute segment counts
	table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )

	surface.DrawPoly( cir )
end

function draw.Meter( x, y, w, h, cur, max, c1, c2 )
    surface.SetDrawColor( c2 )
	surface.DrawRect( x, y, w, h )

    local r = cur/max
    surface.SetDrawColor( c1 )
	surface.DrawRect( x, y, r*w, h )
end

hook.Add( "InitPostEntity", "clMobaInit", function()

	moba = {}
		moba.spells = {}
		moba.build = {}

	for i=1,4 do
		moba.spells[i] = 0
	end

end)

local statHUD = {
    {"Magical Power\t= ", "mpow"},
    {"Physical Power\t= ", "ppow"},
    {"Magical Defense\t= ", "mdef"},
    {"Physical Defense\t= ", "pdef"},
    {"Max Mana\t= ", "mp"}
}

hook.Add( "HUDPaint", "clMobaHUD", function()
    local ply = LocalPlayer()
	local spells = moba.spells
	local god = player_manager.GetPlayerClass( ply )
	local class = baseclass.Get( god )
	local x, y = ScrW() / 2, ScrH() / 2;
    local col = Color( 255, 255, 255, 255 )

    local lvl = ((ply:GetLevel()-1)^1.25)*700
    local nlvl = (ply:GetLevel()^1.25)*700
    local pw, ph = x/1.8, y*0.02
    draw.Meter( x - pw/2, y*1.70, pw, ph, ply:Health(), ply:GetMaxHealth(), Color(0,180,0,255), Color(0,0,0,120) )
    draw.Meter( x - pw/2, y*1.73, pw, ph, ply:Mana(), ply:GetMaxMana(), Color(0,120,200,255), Color(0,0,0,120) )
    draw.Meter( x - pw/2, y*1.97, pw, ph, ply:GetXP()-lvl, nlvl-lvl, Color(200,180,0,120), Color(0,0,0,120) )

    for k,v in pairs(statHUD) do
        draw.DrawText( v[1] .. ply:GetStat(v[2]), "Trebuchet18", x * .46, y * 1.635 + k*14, col, TEXT_ALIGN_LEFT )
    end
    draw.DrawText( "Mana\t\t= " .. ply:Mana(), "Trebuchet18", x * .46, y * 1.635 + 6*14, col, TEXT_ALIGN_LEFT )

    for i = 1, 4 do
		local name = class.spells[ i ]
		local spell = MOBA.Spells[ name ]
		if !spell then return end

		local dist = (i-3) * x * .15 + x*1.015;
		draw.RoundedBox( 0, dist, y * 1.76, x * 0.12, y * 0.20, Color( 0, 0, 0, 120 ) )

		local txt = spell.Name

		if ( spells[i] > CurTime() ) then
			col = Color( 160, 160, 160, 255 )
			txt = math.Round(spells[i] - CurTime(), 1)
        else
            col = Color( 255, 255, 255, 255 )
		end

		draw.DrawText( txt, "Trebuchet24", dist + (x * 0.06), y * 1.835, col, TEXT_ALIGN_CENTER )
	end
end)

local spellBinds = { KEY_1, KEY_2, KEY_3, KEY_4 }
local spellPressed = {}
local function CheckSpellBind(a, key)
    if input.IsKeyDown(key) then
        if spellPressed[key] then return end
        RunConsoleCommand("au_cast", a)
        spellPressed[key] = true
    elseif spellPressed[key] then
        spellPressed[key] = nil
    end
end

net.Receive( "au_spell", function( len, ply )
    local slot = net.ReadInt(4)
	local time = net.ReadFloat()
	moba.spells[slot] = time
end )

hook.Add( "Think", "KeyBindings", function()
	local ply = LocalPlayer()
	local spells = moba.spells
	local god = player_manager.GetPlayerClass( ply )
	local class = baseclass.Get( god )

    for a,k in pairs(spellBinds) do
        CheckSpellBind(a, k)
    end

	--[[if ( input.IsKeyDown( KEY_1 ) ) then

		local name = class.spells[ 1 ]
		local spell = MOBA.Spells[ name ]
		if ( !spells[1] || RealTime() < spells[1] || !spell.CanCast(ply) ) then return end
		net.Start("au_cast")
		net.WriteInt(1, 4)
		net.SendToServer()
		spells[1] = RealTime() + spell.Cooldown

	elseif ( input.IsKeyDown( KEY_2 ) ) then

		local name = class.spells[ 2 ]
		local spell = MOBA.Spells[ name ]
		if ( !spells[2] || RealTime() < spells[2] || !spell.CanCast(ply) ) then return end
		net.Start("au_cast")
		net.WriteInt(2, 4)
		net.SendToServer()
		spells[2] = RealTime() + spell.Cooldown

	elseif ( input.IsKeyDown( KEY_3 ) ) then

		local name = class.spells[ 3 ]
		local spell = MOBA.Spells[ name ]
		if ( !spells[3] || RealTime() < spells[3] || !spell.CanCast(ply) ) then return end
		net.Start("au_cast")
		net.WriteInt(3, 4)
		net.SendToServer()
		spells[3] = RealTime() + spell.Cooldown

	elseif ( input.IsKeyDown( KEY_4 ) ) then

		local name = class.spells[ 4 ]
		local spell = MOBA.Spells[ name ]
		if ( !spells[4] || RealTime() < spells[4] || !spell.CanCast(ply) ) then return end
		net.Start("au_cast")
		net.WriteInt(4, 4)
		net.SendToServer()
		spells[4] = RealTime() + spell.Cooldown

	end]]
end)

hook.Add( "PreDrawOpaqueRenderables", "Draw3D2D", function()
	local ply = LocalPlayer()
	local ang = ply:GetYawAngle()
	local aim = ang:Forward()
	local posxy = ply:GetPosXY()
	local w, h = 2, 400
	local tw = 10
	local m = ply:GetTargeterValue()

	-- Aim line
	cam.Start3D2D( posxy + aim * 8 - ang:Right() * (w/2) + ang:Up(), ang + Angle(0, -90, 0), 1 )

		cam.IgnoreZ( false )

		surface.SetDrawColor( Color( 20, 40, 80, 100 ) )
		surface.DrawRect( -1, 1, w+2, -(h+2) )

		surface.SetDrawColor( Color( 40, 120, 200, 155 ) )
		surface.DrawRect( 0, 0, w, -h )

		--[[surface.SetFont( "DebugFixedSmall" )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( -128, -128 )
		surface.DrawText( tostring( m ) )]]
	cam.End3D2D()

	local circpos = posxy + aim * m - ang:Right() * (tw/2) + ang:Up()
	-- Aim circle
	cam.Start3D2D( circpos, ang, 1 )

		local traceData = {}
		traceData.start = circpos
		traceData.endpos = traceData.start
		traceData.filter = ply
		local trace = util.TraceLine(traceData)

		cam.IgnoreZ( true )
		surface.SetDrawColor( Color( 20, 40, 80, 150 ) )
		draw.Circle( tw/2, tw/2, tw+2, 24 )

		if !trace.StartSolid then
			surface.SetDrawColor( Color( 200, 200, 200, 200 ) )
			draw.Circle( tw/2, tw/2, tw, 24 )
		end
		cam.IgnoreZ( false )

	cam.End3D2D()
end )

hook.Add( "CalcView", "MobaThirdPerson", function( ply, pos, angles, fov )
	local view = {}
	local posxy = Vector(pos.x, pos.y, pos.z*.6 + 36)

	local traceData = {}
	traceData.start = ply:EyePos()
	traceData.endpos = traceData.start - angles:Forward() * 160 + angles:Up() * 20
	traceData.filter = ply

	local trace = util.TraceLine(traceData)

	local tpos = trace.HitPos

	if trace.Fraction < 1.0 then
		tpos = tpos + trace.HitNormal * 5
	end

	view.origin = tpos
	--view.origin = posxy - angles:Forward()*120 + angles:Up()*20
	view.angles = angles
	view.fov = fov
	view.drawviewer = true

	return view
end )

hook.Add( "PreDrawHalos", "ArrowHalo", function()
    halo.Add( ents.FindByClass( "projectile_*" ), Color( 255, 127, 0 ), 2, 2, 4, true, false )
end )

local hide = {
	CHudHealth = true,
	CHudBattery = true,
	CHudWeapon = true,
	CHudWeaponSelection = true
}

hook.Add( "HUDShouldDraw", "HideHUD", function( name )
	if ( hide[ name ] ) then return false end

	-- Don't return anything here, it may break other addons that rely on this hook.
end )
--[[
local avgfps = -1
timer.Create( "TickrateFixer", 1, 0, function()
	local ply = LocalPlayer()

	if avgfps == -1 then
		avgfps = 1/RealFrameTime()
		RunConsoleCommand( "cl_interp_ratio", "1" ) // lerp time ratio.
		RunConsoleCommand( "rate", "30000" ) // max download bandwitdth in bytes/s
		RunConsoleCommand( "cl_interp", "0" ) // final interp if > cl_interp_ratio/cl_updaterate
	end

	avgfps = math.floor(avgfps + 1/RealFrameTime())/2
	local rate = math.floor((avgfps - 5) * .1) * 10
	RunConsoleCommand( "cl_cmdrate", tostring(rate) ) // max command packets per second sent to the server DO NOT SET > FPS
	RunConsoleCommand( "cl_updaterate", tostring(rate) ) // max update packets per second received.
end )
]]
-- Networking
net.Receive( "au_anim", function( len, ply )
	ent = net.ReadEntity()
	ges = net.ReadInt(12)
	act = net.ReadInt(12)
	ent:AnimRestartGesture(ges, act, true)
end )
