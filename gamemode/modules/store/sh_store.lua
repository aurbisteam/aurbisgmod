local meta = FindMetaTable( "Player" )
local items = MOBA.Items
if !SERVER then return end
function meta:BuyItem(class)
	local item = items[class]
	local equclass = self:GetEquippedItem(item.Type)
	if class == equclass then return end

	local equipped, success
	if equclass != "" then
		equipped = items[equclass]
		success = self:SubGold(item.Cost - math.floor(equipped.Cost/1.5))
	else
		success = self:SubGold(item.Cost)
	end

	if success then
		self:EquipItem(class)
	else
		self:ChatPrint("Not enough gold!")
	end
	return success
end

concommand.Add( "au_buyitem", function( ply, cmd, args )
	ply:BuyItem(args[1])
end )
