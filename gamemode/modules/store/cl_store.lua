local frame, goldLabel
concommand.Add( "au_store", function( ply, cmd, args )
	if frame != nil then frame:SetVisible(false) end

	frame = vgui.Create("DFrame")
	frame:SetTitle("Store")
	frame:SetSize(1280, 1024)
	frame:Center()
	frame:MakePopup()
	frame.Paint = function(self, w, h)
		draw.RoundedBox( 0, 0, 0, w, h, Color( 236, 240, 241, 250 ) )
	end
	frame.OnClose = function()
		frame = nil
	end

	local spanel = vgui.Create("DScrollPanel", frame)
	spanel:SetPos(620, 100)
	spanel:SetSize(530, 820)
	spanel.Paint = function(self, w, h)
		draw.RoundedBox( 0, 0, 0, w, h, Color( 189, 195, 199, 250 ) )
	end

	goldLabel = vgui.Create( "DLabel", frame )
	goldLabel:SetPos( 620, 80 )
	goldLabel:SetTextColor( Color( 0, 0, 0 ) )
	goldLabel:SetText( "Gold: " .. LocalPlayer():GetGold() )

	local head = StoreButton("Head", 200, 100, 200, 200)
	head.DoClick = function() AddItems(spanel, "head") end

	local torso = StoreButton("Torso", 200, 310, 200, 200)
	torso.DoClick = function() AddItems(spanel, "torso") end

	local larms = StoreButton("Arms", 110, 310, 80, 240)
	larms.DoClick = function() AddItems(spanel, "arms") end
	local rarms = StoreButton("Arms", 410, 310, 80, 240)
	rarms.DoClick = function() AddItems(spanel, "arms") end

	local lhand = StoreButton(" Primary\nWeapon", 110, 560, 80, 80)
	lhand.DoClick = function() AddItems(spanel, "sword") end
	local rhand = StoreButton("  Secondary\n    Weapon", 410, 560, 80, 80)
	rhand.DoClick = function() AddItems(spanel, "shield") end

	local legs = StoreButton("Legs", 200, 520, 200, 220)
	legs.DoClick = function() AddItems(spanel, "legs") end
	local feet = StoreButton("Feet", 200, 750, 200, 80)
	feet.DoClick = function() AddItems(spanel, "feet") end
end )

function StoreButton(name, x, y, bw, bh)
	local button = vgui.Create( "DButton", frame )
	button:SetText( name )
	button:SetTextColor( Color( 0, 0, 0 ) )
	button:SetPos( x, y )
	button:SetSize( bw, bh )
	button.Paint = function( self, w, h )
		draw.RoundedBox( 0, 0, 0, w, h, Color( 189, 195, 199, 250 ) )
	end
	return button
end

function AddItems(panel, type)
	panel:Clear()
	local i = 0
	for k,v in pairs(MOBA.Items) do
		if v.Type == type then
			local button = panel:Add("DButton")
			button:SetText( v.Name )
			button:SetTextColor( Color( 0, 0, 0 ) )
			button:SetPos( 12 + (i%3)*172, 12 + math.floor(i/3)*172 )
			button:SetSize( 160, 160 )
			button.Paint = function( self, w, h )
				if LocalPlayer():HasItem(k) then
					draw.RoundedBox( 0, 0, 0, w, h, Color( 212, 255, 217, 250 ) )
				else
					draw.RoundedBox( 0, 0, 0, w, h, Color( 236, 240, 241, 250 ) )
				end
			end
			button.DoClick = function()
				RunConsoleCommand("au_buyitem", k)
				timer.Simple( .15, function()
                    goldLabel:SetText("Gold: " .. LocalPlayer():GetGold())
                end )
			end
			i = i + 1
		end
	end
end
