local meta = FindMetaTable( "Player" )
local items = MOBA.Items
local types = {"arms", "head", "legs", "sword", "torso"}

function meta:GetEquippedItem(type)
	return self:GetNWString("inv_" .. type)
end

function meta:HasItem(class)
	local item = items[class]
	return self:GetNWString("inv_" .. item.Type) == class
end

function meta:EquipItem(class)
	local item = items[class]
	-- if !table.HasValue(types, item.Type) then table.insert(types, item.Type) end
	self:SetNWString("inv_" .. item.Type, class)
    self:UpdateStats()
end

function meta:GetItemAddStat(stat)
	local total = 0
	for k,v in pairs(types) do
		local slot = self:GetEquippedItem(v)
		if slot == "" then continue end
		local s = items[slot].Stats[stat]
        if s == nil then continue end
		if s > 0 then total = total + s end
	end
	return total
end
