local meta = FindMetaTable( "Player" )
local items = MOBA.Items

function meta:SetXP(amount)
	self:SetNWInt("stat_xp", amount)
	self:UpdateLevel()
end

function meta:GetXP()
	return self:GetNWInt("stat_xp")
end

function meta:AddXP(amount)
	local xp = self:GetXP()
	self:SetXP(xp + amount)
end

function meta:SetXPToLevel()
	local lvl = self:GetNWInt("stat_lvl")
	local xp = math.floor(700 * math.pow(lvl-1, 1.25))
	self:SetXP(xp)
end

function meta:SetLevel(amount)
	self:SetNWInt("stat_lvl", amount)
	self:LevelUp()
end

function meta:GetLevel()
	return self:GetNWInt("stat_lvl")
end

function meta:UpdateLevel()
	local xp = self:GetXP()
	local lvl = self:GetLevel()
	local nlvl = math.floor(math.pow(xp/700, .8)) + 1
	if nlvl > lvl then
		self:SetLevel(nlvl)
		self:ChatPrint("Level increased to " .. nlvl)
	end
end

function meta:LevelUp()

end

function meta:GetStat(stat)
	local god = player_manager.GetPlayerClass( self )
	local class = baseclass.Get( god )
	local classStat = class.stats[stat]
	local levelStat = class.lvlStats[stat] * self:GetLevel()
	local itemAddStat = self:GetItemAddStat(stat)
	local total = classStat + levelStat + itemAddStat

    -- if stat == "pdef" then print(stat, itemAddStat, total) end
	self:SetNWInt("stat_" .. stat)
	return total
end

function meta:FastStat(stat)
    return self:GetNWInt("stat_" .. stat)
end

function meta:UpdateStats()
    local god = player_manager.GetPlayerClass( self )
	local class = baseclass.Get( god )
	local stats = class.stats
    for k,v in pairs(stats) do
        self:GetStat(k)
    end
end

function meta:Mana()
    return self:GetNWInt("au_mana")
end

function meta:GetMaxMana()
    return self:GetStat("mp")
end

function meta:SetMana(amount)
    local min, max = 0, self:GetMaxMana()
    local result = amount
    if amount >= max then result = max end
    if amount <= min then result = min end
    self:SetNWInt("au_mana", result)
end

function meta:AddMana(amount)
    local mana = self:Mana()
    self:SetMana(mana + amount)
end

function meta:SetHP(amount)
    local min, max = 0, self:GetMaxHealth()
    local result = amount
    if amount >= max then result = max end
    if amount <= min then result = min end
    self:SetHealth(result)
end

function meta:AddHP(amount)
    local hp = self:Health()
    self:SetHP(hp + amount)
end

if !SERVER then return end

timer.Create( "PassiveManaGain", 1, 0, function()
    local plys = player.GetAll()
    for k,v in pairs(plys) do
        v:AddHP(v:GetStat("hps"))
        v:AddMana(v:GetStat("mps"))
    end
end )

timer.Create( "PassiveXPGain", 2, 0, function()
    local plys = player.GetAll()
    for k,v in pairs(plys) do
        if v:GetLevel() < 20 then
            v:AddXP(12)
        end
    end
end )
