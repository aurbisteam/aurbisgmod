local meta = FindMetaTable( "Player" )

function meta:GetYawAngle()
    local eye = self:EyeAngles()
	local ang = Angle(0, eye.y, 0)
    return ang
end

function meta:GetYawVector()
    local ang = self:GetYawAngle()
	local aim = ang:Forward()
    return aim
end

function meta:GetPosXY()
    local pos = self:GetPos()
    return Vector(pos.x, pos.y, 0)
end

function meta:GetTargeterValue()
    local eye = self:EyeAngles()
	local angx = eye.x
	local h = 400
	local mul = math.Clamp(math.Remap(angx, 55, 25, 10, h), 15, h)
    return mul
end

function meta:GetTargeterPos()
    local aim = self:GetYawVector()
    local pxy = self:GetPosXY()
	local mul = self:GetTargeterValue()
    return pxy + aim * mul
end

function meta:TargeterFree()
    local target = self:GetTargeterPos() + Angle(0,1,0):Up()

    local traceData = {}
	traceData.start = target
	traceData.endpos = traceData.start
	traceData.filter = self
	local trace = util.TraceLine(traceData)

	return !trace.StartSolid
end
