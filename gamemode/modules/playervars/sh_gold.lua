local meta = FindMetaTable( "Player" )

function meta:SetGold( amount )
    if !SERVER then return end
	self:SetNWInt("au_gold", amount)
end

function meta:GetGold()
	return self:GetNWInt("au_gold")
end

function meta:AddGold( amount )
	local gold = self:GetGold()
	self:SetGold(gold + amount)
end

function meta:SubGold( amount )
	local gold = self:GetGold()
	local success = gold >= amount
	if success then self:SetGold(gold - amount) end
	return success
end
