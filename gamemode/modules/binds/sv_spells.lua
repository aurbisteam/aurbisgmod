local meta = FindMetaTable( "Player" )
local spells = MOBA.Spells
if !SERVER then return end

function meta:CastSpell(slot)

    slot = tonumber(slot)
    local time = self:GetVar("spell"..slot)
	local god = player_manager.GetPlayerClass( self )
	local class = baseclass.Get( god )
	local name = class.spells[ slot ]
	local spell = spells[ name ]

	if ( !spell || !time || CurTime() < time ) then return end
	if spell.CanCast( self ) then
		spell.OnCast( self )
		self:SetVar("spell"..slot, CurTime() + spell.Cooldown)
        
        net.Start("au_spell")
        net.WriteInt(slot, 4)
    	net.WriteFloat(CurTime() + spell.Cooldown)
    	net.Send(self)
	end



end

concommand.Add( "au_cast", function( ply, cmd, args )
    if !ply:IsPlayer() then return end
	ply:CastSpell(args[1])
end )
