GM.Name = "Aurbis"
GM.Author = "Mutoh"

DeriveGamemode("base")

if ( !MOBA ) then
	MOBA = {}
	MOBA.Characters = {}
	MOBA.Spells = {}
	MOBA.Items = {}
end

local function loadGods( dir )

	for k, v in pairs( file.Find( dir .. "/*.lua", "LUA" ) ) do

		if ( SERVER ) then
			AddCSLuaFile( dir .. "/" .. v )
		end
		include( dir .. "/" .. v )

		local class = string.gsub( v, ".lua", "" )

		print( "MOBA -> Class loaded", class )
	end

end

local function loadSpells( dir )

	for k, v in pairs( file.Find( dir .. "/*.lua", "LUA" ) ) do
		SPELL = {}
		if ( SERVER ) then
			AddCSLuaFile( dir .. "/" .. v )
		end
		include( dir .. "/" .. v )

		local class = string.gsub( v, ".lua", "" )

		MOBA.Spells[ class ] = SPELL

		SPELL.OnInitalize()

		print( "MOBA -> Spell loaded", class )
	end
	SPELL = nil
end

local function loadItems( dir )
	for k, v in pairs( file.Find( dir .. "/*.lua", "LUA" ) ) do
		ITEM = {}
		if ( SERVER ) then
			AddCSLuaFile( dir .. "/" .. v )
		end
		include( dir .. "/" .. v )

		local class = string.gsub( v, ".lua", "" )

		MOBA.Items[ class ] = ITEM

		print( "MOBA -> Item loaded", class )
	end
	ITEM = nil
end

function GM:Initialize()
	self.BaseClass.Initialize(self)
	loadGods("aurbis/gamemode/player_class")
	loadSpells("aurbis/gamemode/spells")
	loadItems("aurbis/gamemode/items")
end

local TEAM_BLUE, TEAM_RED, TEAM_SPEC = 1, 2, 0

team.SetUp( TEAM_SPEC, "Spectators", Color(127, 127, 127))
team.SetUp( TEAM_BLUE, "Blue", Color(50, 150, 250))
team.SetUp( TEAM_RED, "Red", Color(200, 50, 50))
