-- Include
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

local fol = GM.FolderName .. "/gamemode/modules/"
local files, folders = file.Find(fol .. "*", "LUA")

for _, folder in SortedPairs(folders, true) do

    for _, File in SortedPairs(file.Find(fol .. folder .. "/sh_*.lua", "LUA"), true) do
        AddCSLuaFile(fol .. folder .. "/" .. File)
        include(fol .. folder .. "/" .. File)
				print("sv_included " .. fol .. folder .. "/" .. File)
    end

    for _, File in SortedPairs(file.Find(fol .. folder .. "/sv_*.lua", "LUA"), true) do
        include(fol .. folder .. "/" .. File)
				print("sv_included " .. fol .. folder .. "/" .. File)
    end

    for _, File in SortedPairs(file.Find(fol .. folder .. "/cl_*.lua", "LUA"), true) do
        AddCSLuaFile(fol .. folder .. "/" .. File)
				print("sv_included " .. fol .. folder .. "/" .. File)
    end
end

-- Networking
util.AddNetworkString( "au_spell" )
util.AddNetworkString( "au_anim" )

-- Gamemode
local TEAM_BLUE, TEAM_RED, TEAM_SPEC = 1, 2, 0

DEFINE_BASECLASS( "gamemode_base" )

function GM:PlayerInitialSpawn( ply )

	ply:SetTeam(TEAM_RED)
	local color = team.GetColor(ply:Team())
	ply:SetPlayerColor( Vector( color.r/255, color.g/255, color.b/255 ) )

	local ent = ents.Create("tower")
	ent:SetPos( Vector( 80, -1080, 0) )
	ent:Spawn()

	for i = 1,4 do
		ply:SetVar("spell"..i, 0)
	end

	ply:SetGold(9000)

	ply:SetVar("jp_state", false)
	ply:SetVar("jp_starttime", 0.0)
	ply:SetVar("jp_start", Vector(0,0,0))
	ply:SetVar("jp_end", Vector(0,0,0))
	ply:SetVar("jp_height", 0)
	ply:SetVar("jp_speed", 0.0)

    timer.Create( "InitializeStats", 1, 1, function()
        ply:SetMana(ply:GetMaxMana())
    end )

end

function GM:PlayerSpawn( ply )

	player_manager.SetPlayerClass( ply, "god" )
	BaseClass.PlayerSpawn( self, ply )
	--ply:SetCollisionGroup( COLLISION_GROUP_DISSOLVING )
    ply:SetMaxHealth(ply:GetStat("hp"))
    ply:SetWalkSpeed(ply:GetStat("speed"))

end

-- Hooks
function GM:Think()
	local plys = player.GetAll()
	for k,v in pairs(plys) do
		if v:GetVar("jp_state") then
			if !v:GetMoveType(0) then v:SetMoveType(0) end
			local phys = v:GetPhysicsObject()
			if phys:IsGravityEnabled() then phys:EnableGravity( false ) end
			local pos = v:GetVar("jp_start")
			local t = RealTime() - v:GetVar("jp_starttime")
			local h = v:GetVar("jp_height")
			local s = v:GetVar("jp_speed")
			local d = v:GetVar("jp_end") - pos
			local l = d:Length()
			local p = pos + d * (t/s)
			local sq = math.sqrt(h)
			local x = (2*sq/s*t-sq)
			local z = -x*x+h
			v:SetPos(Vector(p.x, p.y, z))
			if (s-t)<=0 then
				p = v:GetPos()
				v:SetMoveType(MOVETYPE_WALK)
				v:SetVar("jp_state", false)
				v:SetPos(Vector(p.x, p.y, 1))
				v:EmitSound("Weapon_Mortar.Impact")
				phys:EnableGravity( false )

				local RP = RecipientFilter()
				RP:AddAllPlayers()

				net.Start("au_anim")
				net.WriteEntity(v)
				net.WriteInt(GESTURE_SLOT_ATTACK_AND_RELOAD, 12)
				net.WriteInt(ACT_MP_JUMP_LAND_PRIMARY, 12)
				net.Send(RP)
			end
		end
	end

end

hook.Add( "Move", "MoveDash", function(ply, mv)
	if(!IsFirstTimePredicted()) then return end
	if ply:GetVar("dash_end") == nil then return end
	if ply:GetVar("dash_end") < RealTime() then return end

	local speed = ply:GetVar("dash_speed") * FrameTime()
	local ang = ply:GetVar("dash_angle")
	local pos = mv:GetOrigin()
	local vel = mv:GetVelocity()

	vel = ang * speed
	pos = pos + vel

	mv:SetVelocity( vel )
	mv:SetOrigin( pos )

	return true
end )

timer.Create( "InitSpawnMinions", 6, 1, function()
    local spawn_minion = {
        ents.FindByName("spawn_minion1")[1]:GetPos(),
        ents.FindByName("spawn_minion2")[1]:GetPos()
    }
    timer.Create( "SpawnMinions", 1, 6, function()
        local minion = ents.Create("npc_minion")
    	minion:SetPos( spawn_minion[1] )
    	minion:Spawn()
    end )
end )

--[[ Networking
net.Receive( "au_cast", function( len, ply )

	local slot = net.ReadInt(4)
	local time = ply:GetVar("spell"..slot)

	local god = player_manager.GetPlayerClass( ply )
	local class = baseclass.Get( god )
	local name = class.spells[ slot ]
	local spell = MOBA.Spells[ name ]

	if ( !spell || !time || RealTime() < time ) then return end
	if spell.CanCast( ply ) then
		spell.OnCast( ply )
		ply:SetVar("spell"..slot, RealTime() + spell.Cooldown)
	end

end )]]
--[[
function GM:PlayerSetModel( ply )
	ply:SetModel("models/player/group01/male_07.mdl")
end]]
